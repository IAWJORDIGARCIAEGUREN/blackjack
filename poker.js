let cartes = [ {archivo:"./PNG/1.png",valor:"2",color:"N"},{archivo:"./PNG/2.png",valor:"2",color:"R"},
{archivo:"./PNG/3.png",valor:"2",color:"R"},{archivo:"./PNG/4.png",valor:"2",color:"N"},{archivo:"./PNG/5.png",valor:"3",color:"N"},
{archivo:"./PNG/6.png",valor:"3",color:"R"},{archivo:"./PNG/7.png",valor:"3",color:"R"},{archivo:"./PNG/8.png",valor:"3",color:"N"},
{archivo:"./PNG/9.png",valor:"4",color:"N"},{archivo:"./PNG/10.png",valor:"4",color:"R"},{archivo:"./PNG/11.png",valor:"4",color:"R"},
{archivo:"./PNG/12.png",valor:"4",color:"N"},{archivo:"./PNG/13.png",valor:"5",color:"N"},{archivo:"./PNG/14.png",valor:"5",color:"R"},
{archivo:"./PNG/15.png",valor:"5",color:"R"},{archivo:"./PNG/16.png",valor:"5",color:"N"},{archivo:"./PNG/17.png",valor:"6",color:"N"},
{archivo:"./PNG/18.png",valor:"6",color:"R"},{archivo:"./PNG/19.png",valor:"6",color:"R"},{archivo:"./PNG/20.png",valor:"6",color:"N"},
{archivo:"./PNG/21.png",valor:"7",color:"N"},{archivo:"./PNG/22.png",valor:"7",color:"R"},{archivo:"./PNG/23.png",valor:"7",color:"R"},
{archivo:"./PNG/24.png",valor:"7",color:"N"},{archivo:"./PNG/25.png",valor:"8",color:"N"},{archivo:"./PNG/26.png",valor:"8",color:"R"},
{archivo:"./PNG/27.png",valor:"8",color:"R"},{archivo:"./PNG/28.png",valor:"8",color:"N"},{archivo:"./PNG/29.png",valor:"9",color:"N"},
{archivo:"./PNG/30.png",valor:"9",color:"R"},{archivo:"./PNG/31.png",valor:"9",color:"R"},{archivo:"./PNG/32.png",valor:"9",color:"N"},
{archivo:"./PNG/33.png",valor:"10",color:"N"},{archivo:"./PNG/34.png",valor:"10",color:"R"},{archivo:"./PNG/35.png",valor:"10",color:"R"},
{archivo:"./PNG/36.png",valor:"10",color:"N"},{archivo:"./PNG/37.png",valor:"J",color:"N"},{archivo:"./PNG/38.png",valor:"J",color:"R"},
{archivo:"./PNG/39.png",valor:"J",color:"R"},{archivo:"./PNG/40.png",valor:"J",color:"N"},{archivo:"./PNG/41.png",valor:"Q",color:"N"},
{archivo:"./PNG/42.png",valor:"Q",color:"R"},{archivo:"./PNG/43.png",valor:"Q",color:"R"},{archivo:"./PNG/44.png",valor:"Q",color:"N"},
{archivo:"./PNG/45.png",valor:"K",color:"N"},{archivo:"./PNG/46.png",valor:"K",color:"R"},{archivo:"./PNG/47.png",valor:"K",color:"R"},
{archivo:"./PNG/48.png",valor:"K",color:"N"},{archivo:"./PNG/49.png",valor:"A",color:"N"},{archivo:"./PNG/50.png",valor:"A",color:"R"},
{archivo:"./PNG/51.png",valor:"A",color:"R"},{archivo:"./PNG/52.png",valor:"A",color:"N"},{archivo:"./PNG/red_back.png",valor:"0",color:"none"}];
 
var gameState = 0;//Es sumaran les coincidencies de les cartes iguals per al jugador
var gameStateE = 0;//Es sumaran les coincidencies de les cartes iguals per al enemic

var jugadas = ["nada","pareja","doble_pareja","trio","-escalera-","color","full","poker","escalera real"];

var apuestas = 10;
document.getElementById("valor_apuesta").innerHTML = apuestas;
var dinero = 500;

var dinero = 1000;
var dinero = parseInt(dinero);

var IdStore = new Array();

//Arrays importants
var mano_jugador = new Array(5);//Array que guarda la ma que te el jugador
var mano_enemigo = new Array(5);//Array que guarda la mà del jugador enemic
var enemigo_cambio = new Array();

//Anticheat code
document.getElementById("but2").style.visibility = "hidden";
document.getElementById("but3").style.visibility = "hidden";
document.getElementById("but4").style.visibility = "hidden";
document.getElementById("but5").style.visibility = "hidden";
document.getElementById("but6").style.visibility = "hidden";
document.getElementById("but7").style.visibility = "hidden";

function jugadorHumanoPrimeraRonda() {
    //Bucle que crea cartes
    var lletres = ["a","b","c","d","e"];
    for (i=0; i < mano_jugador.length; i++){
        var rnd = Math.floor(Math.random() * cartes.length-1);
        var carta = cartes[rnd];
        var imatgeCarta = carta.archivo;
        mano_jugador[i]=carta;
        document.getElementById("cartaimg"+i).src=imatgeCarta;
        document.getElementById("cartaimg"+i).setAttribute("data-valor",i);
        var carta = 0;
    }
    console.log(mano_jugador);
    document.getElementById("but1").style.visibility = "hidden";
    document.getElementById("but2").style.visibility = "visible";
    document.getElementById("but3").style.visibility = "visible";
}

function seleccionarCarta (){
    for (let index = 0; index < 5; index++) {
        document.getElementsByClassName("carta")[index].addEventListener("click",function selecionaCartes() {
            document.getElementsByClassName("carta")[index].classList.add('selected');
        });
    } 
    document.getElementById("but4").style.visibility = "visible";   
}

function desSeleccionarCarta (){
    var idfuera = 0;
    for (let index = 0; index < 5; index++) {
        document.getElementsByClassName("carta")[index].addEventListener("click",function deselecionCartas() {
            document.getElementsByClassName("carta")[index].classList.remove('selected');
        });  
    }
}

function cambiaCartasSeleccionadas(){
    document.getElementById("but2").style.visibility = "hidden";
    document.getElementById("but3").style.visibility = "hidden";
    var IdStore = [];
    var className = document.getElementsByClassName('selected');
    var classnameCount = className.length;//Mirar quina id agafa div o img
    for(var j = 0; j < classnameCount; j++){
        IdStore.push(className[j].id);
    }
    console.log(IdStore);

    if (confirm("Esta seguro que quiere cambiar las cartas selecionadas")){
        for (var index = 0; index < IdStore.length; index++) {
            var canviCarta = IdStore[index];
            var ordreCarta = document.getElementById(canviCarta).getAttribute("data-valor");
            console.log(ordreCarta);
            var rnd = Math.floor(Math.random() * cartes.length-1);
            var carta = cartes[rnd];
            var imatgeCarta = carta.archivo;
            mano_jugador[ordreCarta]=carta;//que concordi la nova carta amb l'ordre de l'array existent
            console.log(IdStore[index]);
            console.log(imatgeCarta);
            console.log(mano_jugador);
            document.getElementById(canviCarta).src=imatgeCarta;
            //document.getElementById(IdStore)[index].src = imatgeCarta;
            document.getElementsByClassName("carta")[index].classList.remove('selected');
        }
        alert("Cartes canviades");//canvia nomes la primera perque index es quada a 0
    };
    //Bucle que canvia cada carta
    document.getElementById("but4").style.visibility = "hidden";  
    document.getElementById("but5").style.visibility = "visible";

    //Treure la classe seleccio
    document.getElementById("cartaimg0").classList.remove('selected');
    document.getElementById("cartaimg1").classList.remove('selected');
    document.getElementById("cartaimg2").classList.remove('selected');
    document.getElementById("cartaimg3").classList.remove('selected');
    document.getElementById("cartaimg4").classList.remove('selected');  
}

//A partir d'aqui comença la part del codi per repartir cartes al enemic

function repartirCartasEnemigo() {
    for (i=0; i < 5; i++){
        var rnd = Math.floor(Math.random() * cartes.length-1);
        var carta = cartes[rnd];
        mano_enemigo[i]=carta;
        document.getElementById("cartaimge"+i).setAttribute("data-valor",i);
    }
    console.log(mano_enemigo);
    //alert("hola");---- Si vols un moment per desplegar l'array de mano_enemigo sense que s'actualitzi, descomenta aixó
}

function comprovarCartasEnemigo() {
    //Guardaem totes les cartes dins de variables especifiques per poder comprovar-les
    //S'iteraran cada un dels objectes separa per separat per veure si te alguna coincidencia
    var carta0 = mano_enemigo[0].valor;
    var carta1 = mano_enemigo[1].valor;
    var carta2 = mano_enemigo[2].valor;
    var carta3 = mano_enemigo[3].valor;
    var carta4 = mano_enemigo[4].valor;

    //Comprovar carta0
    if (carta0 == carta1){
        document.getElementById("cartaimge0").classList.add('selectede');  
    }
    if (carta0 == carta2){
        document.getElementById("cartaimge0").classList.add('selectede');  
    }
    if (carta0 == carta3){
        document.getElementById("cartaimge0").classList.add('selectede');  
    }
    if (carta0 == carta4){
        document.getElementById("cartaimge0").classList.add('selectede');   
    }

   //Comprovar carta1
    if (carta1 == carta0){
        document.getElementById("cartaimge1").classList.add('selectede');  
    }
    if (carta1 == carta2){
        document.getElementById("cartaimge1").classList.add('selectede');  
    }
    if (carta1 == carta3){
        document.getElementById("cartaimge1").classList.add('selectede');  
    }
    if (carta1 == carta4){
        document.getElementById("cartaimge1").classList.add('selectede');  
    }

    //Comprovar carta2
    if (carta2 == carta0){
        document.getElementById("cartaimge2").classList.add('selectede');  
    }
    if (carta2 == carta1){
        document.getElementById("cartaimge2").classList.add('selectede');  
    }
    if (carta2 == carta3){
        document.getElementById("cartaimge2").classList.add('selectede');  
    }
    if (carta2 == carta4){
        document.getElementById("cartaimge2").classList.add('selectede');  
    }

        //Que comprovi quantes coincidencies hi ha
    if (carta3 == carta0){
        document.getElementById("cartaimge3").classList.add('selectede');  
    }
    if (carta3 == carta1){
        document.getElementById("cartaimge3").classList.add('selectede');      
    }
    if (carta2 == carta3){
        document.getElementById("cartaimge3").classList.add('selectede');  
    }
    if (carta3 == carta4){
        document.getElementById("cartaimge3").classList.add('selectede');  
    }

        //Que comprovi quantes coincidencies hi ha
    if (carta4 == carta0){
        document.getElementById("cartaimge4").classList.add('selectede');  
    }
    if (carta4 == carta1){
        document.getElementById("cartaimge4").classList.add('selectede');  
    }
    if (carta4 == carta3){
        document.getElementById("cartaimge4").classList.add('selectede');  
    }
    if (carta3 == carta4){
        document.getElementById("cartaimge4").classList.add('selectede');  
    }
    
}

function cambiarCartasEnemigo() {//Aquesta funció no funciona +- :(
    var ids = ["cartaimge0","cartaimge1","cartaimge2","cartaimge3","cartaimge4"];
    var IdStore = [];
    var coincidencia = [];
    var className = document.getElementsByClassName('selectede');
    var classnameCount = className.length;//Mirar quina id agafa div o img
    for(var j = 0; j < classnameCount; j++){
        IdStore.push(className[j].id);
    }

    for (let index = 0; index < ids.length; index++) {
        var classeSelected = document.getElementById(ids[index]).classList;
        if (classeSelected == "cartae") {
            coincidencia.push(ids[index]);
        }
    }
    console.log(coincidencia);
    console.log(ids); 

    console.log(IdStore);
    console.log(ids);    
    //Copiat, zona de possibles errors
    for (var index = 0; index < coincidencia.length; index++) {
        var canviCarta = coincidencia[index];
        //if (canviCarta )
        var ordreCarta = document.getElementById(canviCarta).getAttribute("data-valor");
        var rnd = Math.floor(Math.random() * cartes.length-1);
        var carta = cartes[rnd];
        mano_enemigo[ordreCarta]=carta;//que concordi la nova carta amb l'ordre de l'array existent   
    }
    console.log(mano_enemigo);
    
    //Treure la class dels div seleccionats
    document.getElementById("cartaimge0").classList.remove('selectede');
    document.getElementById("cartaimge1").classList.remove('selectede');
    document.getElementById("cartaimge2").classList.remove('selectede');
    document.getElementById("cartaimge3").classList.remove('selectede');
    document.getElementById("cartaimge4").classList.remove('selectede');
    
}


function pasarTurnoEnemigo() {//Aquesta funcio aglutinara totes les parts del torn del enemic
    repartirCartasEnemigo();
    comprovarCartasEnemigo();
    cambiarCartasEnemigo();
    document.getElementById("but5").style.visibility = "hidden";
    document.getElementById("but6").style.visibility = "visible";
}

function apuestasJugador() {
    document.getElementById("ap1").addEventListener( "click" , function afegir1() {
        apuestas = parseInt(apuestas) + 1;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });

    document.getElementById("ap5").addEventListener( "click" , function afegir5() {
        apuestas = parseInt(apuestas) + 5;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });

    document.getElementById("ap25").addEventListener( "click" , function afegir25() {
        apuestas = parseInt(apuestas) + 25;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });

    document.getElementById("ap50").addEventListener( "click" , function afegir50() {
        apuestas = parseInt(apuestas) + 50;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });

    document.getElementById("ap100").addEventListener( "click" , function afegir100() {
        apuestas = parseInt(apuestas) + 100;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });

    document.getElementById("clear").addEventListener( "click" , function netejar() {
        apuestas = 10;
        document.getElementById("valor_apuesta").innerHTML = apuestas;
    });
    document.getElementById("but6").style.visibility = "hidden";
    document.getElementById("but7").style.visibility = "visible";
}

function comparacioCartesEnemicPlayer() {
    //En aquesta funcio es comprova qui ha guanyat
    var carta0Valor = mano_jugador[0].valor;
    var carta1Valor = mano_jugador[1].valor;
    var carta2Valor = mano_jugador[2].valor;
    var carta3Valor = mano_jugador[3].valor;
    var carta4Valor = mano_jugador[4].valor;

    //Per la escalera de color
    var carta0Color = mano_jugador[0].color;
    var carta1Color = mano_jugador[1].color;
    var carta2Color = mano_jugador[2].color;
    var carta3Color = mano_jugador[3].color;
    var carta4Color = mano_jugador[4].color;

    if (carta0Valor == "A" && carta1Valor == "K" && carta2Valor == "Q" && carta3Valor == "J" && carta4Valor == "10" ||
    carta4Valor == "A" && carta3Valor == "K" && carta2Valor == "Q" && carta1Valor == "J" && carta0Valor == "10"){
        alert("Tienes escalera real");
        exit();
    }

    if (carta0Valor > carta1Valor && carta1Valor > carta2Valor && carta2Valor > carta3Valor && carta3Valor > carta4Valor && 
    carta0Color == carta1Color && carta1Color == carta2Color && carta2Color == carta3Color && carta3Color == carta4Color){
        alert("Tienes escalera de color");
        exit();
    }

    if (carta0Valor < carta1Valor && carta1Valor < carta2Valor && carta2Valor< carta3Valor && carta3Valor < carta4Valor && 
        carta0Color == carta1Color && carta1Color == carta2Color && carta2Color == carta3Color && carta3Color == carta4Color){
        alert("Tienes escalera de color");
        exit();
    }

    var cartaTest = mano_jugador[0].valor;
    for (let index = 1; index < 5; index++) {
        var cartaComprova = mano_jugador[index].valor;
        if (cartaTest == cartaComprova || carta0Valor == cartaComprova){
            gameState = parseInt(gameState) + 1;
        }
        var cartaTest = mano_jugador[index];
    }

    if (gameState == 5) {
        alert("tienes un full");
    }

    if (gameState == 4) {
        alert("Tienes un poker");
    }

    if (gameState == 3) {
        alert("Tienes un trio");
    }
    if (gameState == 4) {
        alert("Tienes doble pareja");
    }
    if (gameState == 2) {
        alert("Tienes un pareja");
    }

    //Separacio entre enemic i jugado per a la resolucio de jugades
    
    //En aquesta funcio es comprova qui ha guanyat
    var carta0Valor = mano_enemigo[0].valor;
    var carta1Valor = mano_enemigo[1].valor;
    var carta2Valor = mano_enemigo[2].valor;
    var carta3Valor = mano_enemigo[3].valor;
    var carta4Valor = mano_enemigo[4].valor;

    //Per la escalera de color
    var carta0Color = mano_enemigo[0].color;
    var carta1Color = mano_enemigo[1].color;
    var carta2Color = mano_enemigo[2].color;
    var carta3Color = mano_enemigo[3].color;
    var carta4Color = mano_enemigo[4].color;

    if (carta0Valor == "A" && carta1Valor == "K" && carta2Valor == "Q" && carta3Valor == "J" && carta4Valor == "10" ||
    carta4Valor == "A" && carta3Valor == "K" && carta2Valor == "Q" && carta1Valor == "J" && carta0Valor == "10"){
        alert("Tienes escalera real");
        exit();
    }

    if (carta0Valor > carta1Valor && carta1Valor > carta2Valor && carta2Valor > carta3Valor && carta3Valor > carta4Valor && 
        carta0Color == carta1Color && carta1Color == carta2Color && carta2Color == carta3Color && carta3Color == carta4Color){
        alert("Tienes escalera de color");
        exit();
    }
    
    if (carta0Valor < carta1Valor && carta1Valor < carta2Valor && carta2Valor< carta3Valor && carta3Valor < carta4Valor && 
        carta0Color == carta1Color && carta1Color == carta2Color && carta2Color == carta3Color && carta3Color == carta4Color){
        alert("Tienes escalera de color");
        exit();
    }

    var cartaTest = mano_enemigo[0].valor;
    for (let index = 1; index < 5; index++) {
        var cartaComprova = mano_enemigo[index].valor;
        if (cartaTest == cartaComprova){
            gameStateE = parseInt(gameStateE) + 1;
        }
        var cartaTest = mano_enemigo[index];
    }

    if (gameStateE == 5) {
        alert("tienes un full");
        exit();
    }

    if (gameStateE == 4) {
        alert("Tienes un poker");
        exit();
    }

    if (gameStateE == 3) {
        alert("Tienes un trio");
        exit();
    }
    if (gameStateE == 4) {
        alert("Tienes doble pareja");
        exit();
    }
    if (gameStateE == 2) {
        alert("Tienes un pareja");
        exit();
    }
    var ides = ["cartaimge0","cartaimge1","cartaimge2","cartaimge3","cartaimge4"];
    for (let index = 0; index < 5; index++) {
        var cartae = mano_enemigo[index];
        document.getElementById(ides[index]).src=cartae.archivo;        
    }

    //Qui dels dos ha guanyat?
    if (gameState > gameStateE) {
        alert("Has ganado");
        var dinero = parseInt(dinero) + parseInt(apuestas);
        alert("Tienes", dinero,"$");
        exit();
    }else if (gameState < gameStateE) {
        alert("Has perdido");
        var dinero = parseInt(dinero) - parseInt(apuestas);
        alert("Tienes", dinero,"$");
        exit();
    }

    //Quan ningu resulta guanyador per les jugades convencionals, guanya qui tingui la carta més alta

    var cartaTest = mano_jugador[0].valor;
    var cartaAlta = cartaTest;
    var cartaAltaValor = 0;
    for (let index = 0; index < 5; index++) {
        var cartaComprova = mano_jugador[index].valor;
        if (cartaComprova == "A"){
            var cartaComprova = 14;
        }
        if (cartaComprova == "K"){
            var cartaComprova = 13;
        }
        if (cartaComprova == "Q"){
            var cartaComprova = 12;
        }
        if (cartaComprova == "J"){
            var cartaComprova = 12;
        }
        if (cartaAltaValor < cartaComprova ){
            cartaAltaValor++;
        }
    }

    var cartaTest = mano_enemigo[0].valor;
    var cartaAlta = cartaTest;
    var cartaAltaValorE = 0;
    for (let index = 0; index < 5; index++) {
        var cartaComprova = mano_enemigo[index].valor;
        if (cartaComprova == "A"){
            var cartaComprova = 14;
        }
        if (cartaComprova == "K"){
            var cartaComprova = 13;
        }
        if (cartaComprova == "Q"){
            var cartaComprova = 12;
        }
        if (cartaComprova == "J"){
            var cartaComprova = 12;
        }
        if (cartaAltaValorE < cartaComprova ){
            cartaAltaValorE++;
        }
    }

    //Comprova qui ha guanyat finalment
    if (cartaAltaValor > cartaAltaValorE){
        alert("Has ganado, por el metodo de la carta mas alta");
        var dinero = parseInt(dinero) + parseInt(apuestas);
        exit();
    }

    if (cartaAltaValor < cartaAltaValorE){
        alert("Has perdido, por el metodo de la carta mas alta");
        var dinero = parseInt(dinero) - parseInt(apuestas);
        exit();
    }
    document.getElementById("but7").style.visibility = "hidden";
}

/* Aquest troç de codi és inútil i está ple d'errors ves en comte el que còpies
if (confirm("Esta seguro que quiere cambiar las cartas selecionadas")){
        for (var index = 0; index < IdStore.length; index++) {
            var rnd = Math.floor(Math.random() * cartes.length-1);
            var carta = cartes.splice(rnd, 1);
            var imatgeCarta = carta[index].archivo;
            mano_jugador[index]=carta[index];
            document.getElementById(IdStore)[index].setAttribute("src", imatgeCarta);
            document.getElementById(IdStore)[index].setAttribute("data-valor",carta[0].valor);
            document.getElementById(IdStore)[index].setAttribute("data-color",carta[0].color);
            console.log(imatgeCarta,e,mano_jugador);
        }
        alert("Cartes canviades");
    }
*/
/* Per exemple gameState
1 -> Torn jugador - inici del torn del jugador (pot seleccionar)
2 -> Torn IA
3 -> Final de jugada
4 -> Final de partida: en cas de no quedar més diners o s'aixeca el jugador de la taula
*/
//Funció per seleccionar carta quan sigui el torn del jugador

//-------------------------------------------------------Coses a fer------------------------------------
//Ficar que cada vegada que es tregui una carta del array cartes s'esborri per no tenir cartes repetides
//El sistema de apostes seran input radio per selecionar entre quantitas presestablertes, com les fitxes
//Fer que els raonaments del enemic siguin completament lògics, sense capacitat de fer jugades "inventives"